FROM node:latest as builder

WORKDIR /app



COPY . .



RUN npm install -g @angular/cli && npm run ng build
RUN node_modules/.bin/ng build --prod

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=builder /app/dist/time .
 
ENTRYPOINT ["nginx" , "-g", "daemon off;" ]
